import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routerOptions = [
  {
    path: '/signup',
    component: 'SignUp'
  },
  {
    path: '/signin',
    component: 'SignIn'
  },
  {
    path: '/',
    component: 'Home',
    meta: { requiresAuth: true }
  },
  {
    path: '/profile',
    component: 'editProfile'
  },
  {
    path: '/list',
    component: 'List'
  },
  {
    path: '/add_profile',
    component: 'addProfile'
  },
  {
    path: '/edit_profile/:id',
    component: 'EditUser',
    name: 'EditUser',
  }
]

const routes = routerOptions.map(route => {
  return {
    path: route.path,
    component: () => import (`@/components/${route.component}.vue`),
    meta: route.meta
  }
})

export default new Router({
  mode: 'history',
  routes
})