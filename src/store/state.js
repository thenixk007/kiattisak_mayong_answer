export const state = () => ({
    appName: "Front-end Developer",
    user: null,
    error: null,
    loading: false,
    userLogin: null,

    name: null,
    surname: null,
    tel: null,
    age: null,
    address: null,
})