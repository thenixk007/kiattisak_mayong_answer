import firebase from 'firebase/app'
import 'firebase/firestore'

if (!firebase.apps.length) {
    let Config = {
        apiKey: "AIzaSyD7HMaOyDtdwv4SrbLM8Hj4khszCpww7V4",
        authDomain: "example-front-end-149c0.firebaseapp.com",
        databaseURL: "https://example-front-end-149c0.firebaseio.com",
        projectId: "example-front-end-149c0",
        storageBucket: "",
        messagingSenderId: "851758251197",
        appId: "1:851758251197:web:6d7879020f47a7c8"
    }
    firebase.initializeApp(Config)
}
const db = firebase.firestore()

export default db;
